package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"path"
	"strings"
	"fmt"
)

var forcePtr = flag.Bool("f", false, "force operation, ignore errors")
var dryrunPtr = flag.Bool("n", false, "dry run, print out operations")

func move(ff []string, t string) ([]string, error) {
	var rr []string
	for _, f := range ff {
		r := path.Join(t, filepath.Base(f))
		rr = append(rr, r)
		var err error
		if *dryrunPtr {
			fmt.Println("mv " + f + " " + r);
		} else {
			err = os.Rename(f, r)
		}
		if err != nil {
			if *forcePtr {
				log.Println(err)
			} else {
				return nil, err
			}
		}
	}
	return rr, nil
}

func readdir(d string) ([]string, error) {
	ff, err := ioutil.ReadDir(d)
	if err != nil {
		return nil, err
	}
	ss := make([]string, len(ff))
	for i, f := range ff {
		ss[i] = path.Join(d, f.Name())
	}
	return ss, nil
}

func filter(ss []string, keep func(string) bool) []string {
	vss := make([]string, 0)
	for _, v := range ss {
		if keep(v) {
			vss = append(vss, v)
		}
	}
	return vss
}

func rel(p1, p2 string) bool {
	if p1 != "/" {
		p1 = strings.TrimSuffix(p1, "/")
	}
	if p2 != "/" {
		p2 = strings.TrimSuffix(p2, "/")
	}
	return strings.HasPrefix(p1, p2)
}

func TempDir(s1 string, s2 string) (string,error) {
	if *dryrunPtr {
		d := path.Join(s1, s2 + "XXXXX")
		fmt.Println("mkdir -p " + d)
		return d, nil
	} else {
		return ioutil.TempDir(s1, s2)
	}
}

func Remove(s1 string) error {
	if *dryrunPtr {
		fmt.Println("rm -rf " + s1)
		return nil
	} else {
		return os.Remove(s1)
	}
}

func main() {
	flag.Parse()
	aa := flag.Args()
	s1 := aa[0]
	s2 := aa[1]
	ff1, err := readdir(s1)
	if err != nil {
		panic(err)
	}
	if rel(s2, s1) {
		ff1 = filter(ff1, func(v string) bool {
			return !rel(s2, v)
		})
	}
	ff2, err := readdir(s2)
	if err != nil {
		panic(err)
	}
	if rel(s1, s2) {
		ff2 = filter(ff2, func(v string) bool {
			return !rel(s1, v)
		})
	}
	tmp, err := TempDir(s1, "tmp")
	if err != nil {
		panic(err)
	}
	defer Remove(tmp)
	ff1, err = move(ff1, tmp)
	if err != nil {
		panic(err)
	}
	if *dryrunPtr {
		fmt.Println()
	}
	ff2, err = move(ff2, s1)
	if err != nil {
		panic(err)
	}
	if *dryrunPtr {
		fmt.Println()
	}
	ff1, err = move(ff1, s2)
	if err != nil {
		panic(err)
	}
}
