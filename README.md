# swap

Swap folders content with static binary file. Helps you swap OS between eachother.

I have two OS for testing ubuntu and debian on the same single installation partition.
One installed at root (/) and another installed as /old. To switch between OS safer
option would be to boot into rescue USB and move files manually from one folder to
another. But it takes addtinional time and requiring connecting external usb device to your PC.
To save time you have another option using hot-swaping files on live running
OS's.

For hot-swapping running OS from /old folder into / you have to move files manually in few steps.
First you have create temp folder, then move / files into temp folder, then move all files from /old
into /, then moving temp fils into /old.

But moving files manually using mv command will likly crash your OS instantly (since libraries
and linked binares are gone and will not be found by kernel on lookup operation). You have to do swap opereation
at once by statically compiled binary (without any libraries dependencies) and then reboot imidiatly after swap operation.

As result you will save one reboot, safe disk space (since you don't have to keep separate
partitions for each OS) and complete swap OS operation just by one click / reboot!
Trick works!

Tested OS:

  * Debian 10 (ext4/btrfs)
  * Ubuntu 20.04 (ext4/btrfs)

Move operation completes in 4 setps:

    swap -f 1/ 2/

expands into:

  1) mkdir 1/tmp11111
  2) move 1/* 1/tmp11111
  3) move 2/* 1/
  4) move 1/tmp11111/* /2
  5) rm -rf 1/tmp11111

## Install

    go get -u -ldflags "-linkmode external -extldflags -static" -buildmode pie gitlab.com/axet/swap

## Manual build

    make

## Usage

Flags -f - force move, ignore errors while attempting to move /dev, /proc, /sys... 

    swap -f / /old

then restore your /home folder by calling:

    mv /old/home /

and reboot the machine. It works!

Flag -n dryrun, print out move commands:

    swap -n / /old

## Helper script

Helper script can be found here: https://gitlab.com/axet/homebin/-/tree/debian/swapos
